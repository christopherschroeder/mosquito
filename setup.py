from setuptools import setup, find_packages
setup(
    name = "gi-mosquito",
    version = "1.0",
    author = "Christopher Schröder and Sven Rahmann",
    author_email = "christopher.schroeder@tu-dortmund.de",
    long_description=__doc__,
    license = "MIT",
    url = "https://bitbucket.org/christopherschroeder/moquito",
    packages=find_packages(),
    py_modules = ["run"],
    package_data={
        'mosquito': 'mosquito/*'},
    include_package_data=True,
    zip_safe=False,
    install_requires=[],
    entry_points = {"console_scripts": [
                        "mosquito = mosquito.mosquito:main"
                    ]},
    classifiers = [
        "Topic :: Scientific/Engineering :: Bio-Informatics",
        "Framework :: Flask",
        "Environment :: Web Environment",
        "Intended Audience :: Science/Research"]
)
